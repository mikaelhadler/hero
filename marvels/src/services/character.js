import { http, publicKey, privateKey } from './http'
import md5 from 'md5'

export const getCharacters = () => {
  const ts = Date.now()
  const apiKey = md5(ts + privateKey + publicKey)
  return http.get(`characters?ts=${ts}&apikey=${publicKey}&hash=${apiKey}`)
}
